class DashboardController < ApplicationController
  before_action :authenticate_user!

  authorize_resource :class => false

  def show
    @cards = [[],[]]
    current_user.cards.order(:created_at).each_with_index do |card, i|
      i.even? ? @cards[0] << card : @cards[1] << card
    end
  end
end
