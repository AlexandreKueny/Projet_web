class NotificationsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def update
    @notification.update(notification_params)
  end

  def dismiss_all
    current_user.notifications.unread.update(read: true)
  end

  def history
    @notifications = current_user.notifications.all.order(created_at: :desc)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_notification
    @notification = Notification.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def notification_params
    params.require(:notification).permit(:read)
  end
end
