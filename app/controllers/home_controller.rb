class HomeController < ApplicationController
  before_action :check_redirect

  private

  def check_redirect
    redirect_to user_signed_in? ? dashboard_path : new_user_session_path
  end
end
