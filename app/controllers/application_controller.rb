class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  check_authorization :unless => :devise_controller?

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def after_sign_in_path_for(resource)
    dashboard_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden }
      format.html { head :forbiden }
      format.js   { head :forbidden }
    end
  end
end
