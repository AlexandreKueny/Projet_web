class DocumentationController < ApplicationController
  before_action :authenticate_user!

  authorize_resource :class => false

  def show
  end
end
