class FieldsController < ApplicationController
  before_action :authenticate_user!

  # before_action :set_field, only: [:clear]

  before_action :set_device, only: [:index, :create, :update, :destroy, :clear]

  load_and_authorize_resource :through => :device

  layout false

  def create
    @field.save
    render partial: 'field_row', locals: {field: @field}
  end

  def update
    @field.update(field_params)
  end

  def destroy
    id = @field.id
    @field.destroy
    render json: {id: id}
  end

  def clear
    set_field
    @field.values.destroy_all
    render json: {cleared: true}
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_field
    @field = Field.find(params[:id])
  end

  def set_device
    @device = Device.find(params[:device_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def field_params
    params.fetch(:field, {}).permit(:name, :unit, :enabled)
  end
end
