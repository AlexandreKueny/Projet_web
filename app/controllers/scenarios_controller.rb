class ScenariosController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  wrap_parameters false

  def new
    render layout: false
  end

  def create
    scenario = current_user.scenarios.create(scenario_params)
    params[:scenario][:conditions].each do |c|
      scenario.conditions.create(field_id: c[:field_id], field_position: c[:position], field_threshold: c[:threshold])
    end
    params[:scenario][:actions].each do |a|
      scenario.actions.create(action_type: a[:type], email_object: a[:email_object], email_text: a[:email_text], notification_title: a[:notification_title], notification_text: a[:notification_text], notification_level: a[:notification_level])
    end
    respond_to do |format|
      format.html {render partial: 'scenario_row', locals: {scenario: scenario}}
      format.json {render :show, status: :created, location: scenario}
    end
  end

  def edit
    respond_to do |format|
      format.json {render file: 'scenarios/edit.json'}
    end
  end

  def update
    @scenario.update(scenario_params)
    unless scenario_params[:enabled] != nil
      @scenario.conditions.clear
      @scenario.actions.clear
      params[:scenario][:conditions].each do |c|
        @scenario.conditions.create(field_id: c[:field_id], field_position: c[:position], field_threshold: c[:threshold])
      end
      params[:scenario][:actions].each do |a|
        @scenario.actions.create(action_type: a[:type], email_object: a[:email_object], email_text: a[:email_text])
      end
    end
  end

  def destroy
    id = @scenario.id
    @scenario.destroy
    render json: {id: id}
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_scenario
    @scenario = Scenario.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def scenario_params
    params.require(:scenario).permit(:name, :enabled, conditions: [], actions: [])
  end
end
