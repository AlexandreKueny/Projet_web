class ValuesController < ApplicationController
  acts_as_token_authentication_handler_for User

  before_action :authenticate_user!

  before_action :set_field, only: [:index, :show, :create, :destroy]

  load_and_authorize_resource :through => :field

  def create
    @value.save
    render :show, status: :created
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_value
    @value = Value.find(params[:id])
  end

  def set_field
    @field = Field.find(params[:field_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def value_params
    params.fetch(:value, {}).permit(:value)
  end
end
