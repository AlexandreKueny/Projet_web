class DevicesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  # POST /devices
  # POST /devices.json
  def create
    # @device = current_user.devices.create(device_params)
    @device.save
    respond_to do |format|
      format.html {render partial: 'device_row', locals: {device: @device}}
      format.json {render :show, status: :created, location: @device}
    end
  end

  # PATCH/PUT /devices/1
  # PATCH/PUT /devices/1.json
  def update
    @device.update(device_params)
    respond_to do |format|
      format.html {head :ok}
      format.json {render :show, status: :created, location: @device}
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    id = @device.id
    @device.destroy
    render json: {id: id}
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_device
    @device = Device.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def device_params
    params.fetch(:device, {}).permit(:name, :enabled)
  end

end
