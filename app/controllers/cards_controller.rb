class CardsController < ApplicationController
  before_action :authenticate_user!

  load_and_authorize_resource

  layout false

  def show
    render partial: 'card', object: @card
  end

  def create
    # @card = current_user.cards.create(card_params)
    @card.save
    render partial: 'card', object: @card
  end

  def update
    @card.update(card_params)
    unless card_params[:field_ids]
      @card.fields.clear
    end
    render partial: 'card', object: @card
  end

  def destroy
    id = @card.id
    @card.destroy
    render json: {id: id}
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_card
    @card = Card.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def card_params
    params.fetch(:card, {}).permit(:title, :card_type, field_ids: [])
  end
end
