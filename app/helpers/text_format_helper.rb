module TextFormatHelper

  def reformat(text, scenario)
    conditions = ""
    scenario.conditions.each do |c|
      conditions << "#{c.field.device.name}:#{c.field.name} is #{c.field_position} #{c.field_threshold}\n"
    end
    return text.gsub('/S', scenario.name).gsub('/C', conditions)
  end
end
