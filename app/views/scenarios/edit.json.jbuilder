json.modal render(file: 'scenarios/edit.html')
json.conditions @scenario.conditions do |c|
  json.id c.id
  json.device do
    json.name Field.find(c.field_id).device.name
    json.id Field.find(c.field_id).device.id
  end
  json.field do
    json.name Field.find(c.field_id).name
    json.id Field.find(c.field_id).id
  end
  json.position c.field_position
  json.threshold c.field_threshold
end
json.actions @scenario.actions do |a|
  json.id a.id
  json.type a.action_type
  json.notification do
    json.title ''
    json.text ''
    json.level ''
  end
  json.email do
    json.object a.email_object
    json.text a.email_text
  end
end