json.id @scenario.id
json.html render(partial: 'scenarios/scenario_row', locals: {scenario: @scenario})