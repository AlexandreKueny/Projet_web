json.extract! field, :id, :name, :unit, :created_at, :updated_at
json.url device_field_url(field.device, field, format: :json)