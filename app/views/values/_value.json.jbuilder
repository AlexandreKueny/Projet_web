json.extract! value, :id, :value, :created_at
json.url device_field_value_url(value.field.device, value.field, value, format: :json)
