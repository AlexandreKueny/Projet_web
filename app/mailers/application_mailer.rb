class ApplicationMailer < ActionMailer::Base
  default from: '"Projet-web" <projet-web@alexandrekueny.fr>'
  layout 'mailer'
end
