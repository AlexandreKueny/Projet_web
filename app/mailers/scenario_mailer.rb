class ScenarioMailer < ApplicationMailer
  include TextFormatHelper

  def notify_email(action)
    @scenario = action.scenario

    @email_text = reformat(action.email_text, @scenario)
    mail(to: @scenario.user.email, subject: reformat(action.email_object, @scenario))

  end

end
