class Notification < ApplicationRecord
  belongs_to :user

  scope :unread, -> { where read: false }

  def unread?
    self.read != true
  end

end
