class User < ApplicationRecord
  acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable,
         :confirmable, :lockable

  has_many :devices, dependent: :destroy
  has_many :cards, dependent: :destroy
  has_many :scenarios, dependent: :destroy
  has_many :notifications, dependent: :destroy

  def reset_authentication_token!
    self.authentication_token = nil
    save
  end

end
