class Card < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :fields
  before_create :check_title
  before_destroy { fields.clear }

  def check_title
    self.title = 'Default title' if title == ''
  end

end