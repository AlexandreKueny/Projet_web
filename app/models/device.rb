class Device < ApplicationRecord
  belongs_to :user
  has_many :fields, dependent: :destroy

  scope :enabled, -> { where enabled: true}

  before_create :check_name

  def check_name
    self.name = 'Default device name' if name == ''
  end
end
