class Scenario < ApplicationRecord
  belongs_to :user
  has_many :conditions, dependent: :destroy
  has_many :actions, dependent: :destroy

  scope :concern_field, ->(field) { Scenario.includes(:conditions).where(conditions: {field_id: field.id}) }
  scope :enabled, -> { where enabled: true }

  include TextFormatHelper

  def check
    self.conditions.map {|c| c.check}.delete_if {|v| v == false}.size == self.conditions.size
  end

  def execute
    p "execute scenario #{self.id}"
    self.actions.each do |a|
      if a.action_type == 'email'
        ScenarioMailer.notify_email(a).deliver_later
      elsif a.action_type == 'notification'
        scenario = a.scenario
        scenario.user.notifications.create(title: reformat(a.notification_title, scenario), text: reformat(a.notification_text, scenario), level: a.notification_level)
      end
    end
    return
  end

end
