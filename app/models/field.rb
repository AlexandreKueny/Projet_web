class Field < ApplicationRecord
  belongs_to :device
  has_many :values, dependent: :destroy
  has_and_belongs_to_many :cards
  has_many :conditions


  scope :enabled, -> { where enabled: true}

  before_create :check_name
  before_destroy { cards.clear }

  def check_name
    self.name = 'Default field name' if name == ''
  end

  def last_value
    self.values.order(:created_at).last
  end
end
