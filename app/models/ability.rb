class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    alias_action :create, :read, :update, :destroy, to: :crud

    can :crud, Device, user_id: user.id
    can :crud, Field, device: { user_id: user.id }
    can :clear, Field, device: { user_id: user.id }
    can :crud, Value, field: { device: { user_id: user.id } }
    can :crud, Card, user_id: user.id
    can :crud, Scenario, user_id: user.id
    can :update, Notification, user_id: user.id
    can :dismiss_all, Notification, user_id: user.id
    can :history, Notification, user_id: user.id
    can :show, :dashboard
    can :show, :documentation
  end
end
