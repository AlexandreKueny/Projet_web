class Value < ApplicationRecord
  belongs_to :field

  after_create :check_scenarios

  def check_scenarios
    self.field.device.user.scenarios.enabled.concern_field(self.field).each do |scenario|
      scenario.execute if scenario.check
    end
  end
end

