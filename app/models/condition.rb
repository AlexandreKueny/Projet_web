class Condition < ApplicationRecord
  belongs_to :scenario
  belongs_to :field

  def check
    case self.field_position
      when 'below'
        return self.field.values.order(:created_at).last.value < self.field_threshold
      when 'equal'
        return self.field.values.order(:created_at).last.value == self.field_threshold
      when 'over'
        return self.field.values.order(:created_at).last.value > self.field_threshold
      else
        return false
    end
  end

end
