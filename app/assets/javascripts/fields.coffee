jQuery ->

  $('#remote-modal-large').on 'click', '#create-field-btn', ->
    if $('#new-field-name').val() != ''
      $.post $(this).data().url, {
        field:
          name: $('#new-field-name').val(),
          unit: $('#new-field-unit').val(),
      }, (data) ->
        $('#fields').find('tr:last').before(data)
        $('#fields').find('tr:last').prev().foundation()
        $('#new-field-name').val('')
        $('#new-field-unit').val('')

  $('#remote-modal-large').on 'click', '.field-state-switch', ->
    $.put $(this).data().url, {
      field:
        enabled: $(this).is(':checked')
    }
  $('#remote-modal-large').on 'click', '.delete-field-btn', ->
    if confirm('Are you sure ?')
      $.delete $(this).data().url, (data) ->
        $('#' + data.id).remove()

  $('#remote-modal-large').on 'click', '.clear-field-btn', ->
    if confirm('Are you sure ?')
      $.delete $(this).data().url