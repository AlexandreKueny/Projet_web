jQuery ->

  create_input = (ids, d) ->
    if ids.includes(d.id)
      $('#fields-boxes').append('<input type="checkbox" class="field-box" value="' + d.id + '"' + 'checked' + '><label>' + d.name + '</label>')
    else
      $('#fields-boxes').append('<input type="checkbox" class="field-box" value="' + d.id + '"><label>' + d.name + '</label>')

  $('#remote-modal').on 'change', '#device_id', ->
    $('#fields-boxes').html('')
    $.get '/devices/' + this.value + '/fields.json', (data) ->
      if data.length > 0
        selected_ids = []
        if $('#device_id').data().selected_ids
          selected_ids = $('#device_id').data().selected_ids.split(' ')
        create_input(selected_ids, field) for field in data
      else
        $('#fields-boxes').html('No available fields')



  $('#remote-modal').on 'click', '#create-card-btn', ->
    if $('#card-title').val() != '' and $('#card_type').val() != ''
      field_ids = []
      ((field_ids.push(box.value)) if box.checked) for box in $('.field-box')
      $.post $(this).data().url, {
        card:
          title: $('#card-title').val(),
          card_type: $('#card_type').val(),
          field_ids: field_ids
      }, (data) ->
        $('#remote-modal').foundation('close')
        if $('#cards-left').children().length > $('#cards-right').children().length
          $('#cards-right').append('<div class="cell">' + data + '</div>')
        else
          $('#cards-left').append('<div class="cell">' + data + '</div>')

  $('#page-content').on 'click', '.edit-card-btn', ->
    $.get $(this).data().url, (data) ->
      $('#remote-modal').html(data).foundation('open')

  $('#remote-modal').on 'click', '#update-card-btn', ->
    if $('#card-title').val() != '' and $('#card_type').val() != ''
      btn = $(this)
      field_ids = []
      ((field_ids.push(box.value)) if box.checked) for box in $('.field-box')
      $.put $(this).data().url, {
        card:
          title: $('#card-title').val(),
          card_type: $('#card_type').val(),
          field_ids: field_ids
      }, (data) ->
        $('#remote-modal').foundation('close')
        $('#' + btn.data().card_id).parent().html(data)

  $('#remote-modal').on 'click', '#delete-card-btn', ->
    if confirm('Are you sure ?')
      $.delete $(this).data().url, (data) ->
        $('#remote-modal').foundation('close')
        $('#' + data.id).remove()