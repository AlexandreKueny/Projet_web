jQuery ->

  $('#new-device-name').on 'keyup', (e) ->
    if e.keyCode == 13
      $('#create-device-btn').click()

  $('#page-content').on 'click', '#create-device-btn', ->
    if $('#new-device-name').val() != ''
      $.post $(this).data().url, {
        device:
          name: $('#new-device-name').val()
      }, (data) ->
        $('#devices').find('tr:last').before(data)
        $('#devices').find('tr:last').prev().foundation()
        $('#new-device-name').val('')

  $('#page-content').on 'click', '.edit-device-btn', ->
    $.get $(this).data().url, (data) ->
      $('#remote-modal-large').html(data).foundation('open')

  $('#page-content').on 'click', '.device-state-switch', ->
    $.put $(this).data().url, {
      device:
        enabled: $(this).is(':checked')
    }

  $('#page-content').on 'click', '.delete-device-btn', ->
    if confirm('Are you sure ?')
      $.delete $(this).data().url, (data) ->
        $('#' + data.id).remove()