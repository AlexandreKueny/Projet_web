jQuery ->

  $('#notifications-bar').on 'click', '.dismiss-notification-btn', ->
    $.put $(this).data().url, {
      notification:
        read: true
    }
    $('.notification-item#' + $(this).closest('li')[0].id ).removeClass('unread')
    $('.notification-item#' + $(this).closest('li')[0].id + ' a span.unread-marker').hide()
    $(this).closest('li').remove()
    newCount = parseInt($('#notifications-counter').html()) - 1
    if newCount > 0
      $('#notifications-counter').html(newCount.toString())
    else
      $('#notifications-counter').hide()

  $('#notifications-bar').on 'click', '#clear-all-notifications-btn', ->
    $.delete $(this).data().url
    $('#notifications-list').empty()
    $('#notifications-counter').hide()
    for elem in $('#notifications-history .notification-item.unread')
      $(elem).removeClass('unread')
      $(elem).find('a span.unread-marker').hide()

  $('#page-content').on 'click', '.notification-item.unread', ->
    $.put $('#notifications-list #' + $(this).data().id + ' .dismiss-notification-btn').data().url, {
      notification:
        read: true
    }
    $('#notifications-list #' + $(this).data().id).remove()
    $(this).removeClass('unread')
    $('#' + $(this).data().id + ' a span.unread-marker').hide()
    newCount = parseInt($('#notifications-counter').html()) - 1
    if newCount > 0
      $('#notifications-counter').html(newCount.toString())
    else
      $('#notifications-counter').hide()