jQuery ->

  conditions = []
  actions = []

  $('#page-title').on 'click', '#new-scenario-btn', ->
    conditions = []
    actions = []
    $.get $(this).data().url, (data) ->
      $('#remote-modal').html(data).foundation('open')
      $('#remote-modal').foundation()

  $('#remote-modal').on 'change', '#scenario_device_id', ->
    $.get '/devices/' + this.value + '/fields.json', (data) ->
      if data.length > 0
        $('#scenario_field_id option:gt(0)').remove()
        $('#scenario_field_id').append($("<option></option>").attr("value", field.id).text(field.name)) for field in data

  $('#remote-modal').on 'click', '#add-condition-btn', ->
    if $('#scenario_device_id').val() != null and $('#scenario_field_id').val() != null and $('#scenario_field_position ').val() != null and $('#scenario_field_threshold').val() != ''
      date = new Date
      conditions.push({
        id: date.getTime(),
        device: {
          name: $('#scenario_device_id option:selected').text(),
          id: $('#scenario_device_id').val()
        },
        field: {
          name: $('#scenario_field_id option:selected').text(),
          id: $('#scenario_field_id').val()
        },
        position: $('#scenario_field_position ').val(),
        threshold: $('#scenario_field_threshold').val()
      })
      $('#conditions').empty()
      $('#conditions').append('<li id="' + c.id + '"><button class="delete-condition-btn" type="button" style="margin-right: 5px" data-id="' + c.id + '"><i class="fas fa-times" aria-hidden="true"></i></button>' + c.device.name + ':' + c.field.name + ' is ' + c.position + ' ' + c.threshold + '</li>') for c in conditions
      $('#scenario_device_id').prop('selectedIndex',0)
      $('#scenario_field_id').prop('selectedIndex',0)
      $('#scenario_field_position ').prop('selectedIndex',0)
      $('#scenario_field_threshold').val('')

  $('#remote-modal').on 'change', '#action_type', ->
    $('.action-selector').addClass('hide')
    $('#' + this.value).removeClass('hide')

  $('#remote-modal').on 'click', '#add-action-btn', ->
    if ($('#email_object').val() != '' and $('#email_text').val() != '') or ($('#notification_title').val() != '' and $('#notification_text').val() != '' and $('#notification_level').val() != null)
      date = new Date
      actions.push({
        id: date.getTime(),
        type: $('#action_type').val(),
        notification: {
          title : $('#notification_title').val(),
          text : $('#notification_text').val(),
          level : $('#notification_level').val(),
        },
        email: {
          object: $('#email_object').val(),
          text: $('#email_text').val()
        }
      })
      $('#actions').empty()
      $('#actions').append('<li id="' + a.id + '"><button class="delete-action-btn" type="button" style="margin-right: 5px" data-id="' + a.id + '"><i class="fas fa-times" aria-hidden="true"></i></button>' + (if a.type == 'email' then 'Email : ' + a.email.object else 'Notification : ' + a.notification.level + ' : ' + a.notification.title) + '</li>') for a in actions
      $('#notification_title').val('')
      $('#notification_text').val('')
      $('#notification_level').prop('selectedIndex',0)
      $('#email_object').val('')
      $('#email_text').val('')
      $('#action_type').prop('selectedIndex',0)
      $('.action-selector').addClass('hide')

  $('#remote-modal').on 'click', '.delete-condition-btn', ->
    i = conditions.findIndex((c) => c.id == $(this).data().id)
    if i != -1
      conditions.splice(i, 1)
      $('#' + $(this).data().id).remove()
      if conditions.length == 0
        $('#conditions').append('<li>No conditions</li>')

  $('#remote-modal').on 'click', '.delete-action-btn', ->
    i = actions.findIndex((a) => a.id == $(this).data().id)
    if i != -1
      actions.splice(i, 1)
      $('#' + $(this).data().id).remove()
      if actions.length == 0
        $('#actions').append('<li>No actions</li>')

  $('#remote-modal').on 'click', '#create-scenario-btn', ->
    if $('#scenario-name').val() != '' and conditions.length > 0 and actions.length > 0
      $.postJSON $(this).data().url, {
        scenario:
          name: $('#scenario-name').val(),
          conditions: conditions.map((c) => {field_id: c.field.id, position: c.position, threshold: c.threshold}),
          actions: actions.map((a) => {type: a.type, email_object: a.email.object, email_text: a.email.text, notification_title: a.notification.title, notification_text: a.notification.text, notification_level: a.notification.level})
      },
      'html',
      (data) ->
        $('#scenarios').append(data)
        $('#scenarios').find('tr:last').foundation()
        $('#remote-modal').foundation('close')

  $('#remote-modal').on 'click', '.next-tab', ->
    tabSwitch()

  tabSwitch = ->
    tabs = $('ul.tabs')
    tab_content = $('.tabs-content')
    currentTab = tabs.find('.is-active')
    currentPanel = tab_content.find('.is-active')
    currentTab.removeClass('is-active').find('[aria-selected]').attr 'aria-selected', false
    if currentTab.next('li').length != 0
      currentTab.next('li').addClass('is-active').find('[aria-selected]').attr 'aria-selected', true
    else
      tabs.find('li:first').addClass('is-active').find('[aria-selected]').attr 'aria-selected', true
    if currentPanel.next('.tabs-panel').length != 0
      currentPanel.removeClass('is-active').next('.tabs-panel').addClass 'is-active'
    else
      currentPanel.removeClass 'is-active'
      tab_content.find('.tabs-panel:first').addClass 'is-active'

  $('#page-content').on 'click', '.scenario-state-switch', ->
    $.put $(this).data().url, {
      scenario:
        enabled: $(this).is(':checked')
    }

  $('#page-content').on 'click', '.edit-scenario-btn', ->
    $.get $(this).data().url + '.json', (data) ->
      conditions = data.conditions
      actions = data.actions
      $('#remote-modal').html(data.modal).foundation('open')
      $('#remote-modal').foundation()

  $('#remote-modal').on 'click', '#update-scenario-btn', ->
    if $('#scenario-name').val() != '' and conditions.length > 0 and actions.length > 0
      $.put $(this).data().url, {
          scenario:
            name: $('#scenario-name').val(),
            conditions: conditions.map((c) => {field_id: c.field.id, position: c.position, threshold: c.threshold}),
            actions: actions.map((a) => {type: a.type, email_object: a.email.object, email_text: a.email.text, notification_title: a.notification.title, notification_text: a.notification.text, notification_level: a.notification.level})
        }, (data) ->
          $('#' + data.id).replaceWith(data.html)
          $('#scenarios').find('tr:last').foundation()
          $('#remote-modal').foundation('close')

  $('#page-content').on 'click', '.delete-scenario-btn', ->
    if confirm('Are you sure ?')
      $.delete $(this).data().url, (data) ->
        $('#' + data.id).remove()

