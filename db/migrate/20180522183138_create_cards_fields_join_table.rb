class CreateCardsFieldsJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :cards_fields, id: false do |t|
      t.belongs_to :card, type: :uuid, index: true, foreign_key: true
      t.belongs_to :field, type: :uuid, index: true, foreign_key: true
    end
  end
end
