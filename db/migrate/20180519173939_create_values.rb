class CreateValues < ActiveRecord::Migration[5.1]
  def change
    create_table :values, id: :uuid do |t|
      t.float :value
      t.belongs_to :field, type: :uuid, index: true, foreign_key: true

      t.timestamps
    end
  end
end
