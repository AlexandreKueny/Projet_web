class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications, id: :uuid do |t|
      t.string :title
      t.string :text
      t.string :level
      t.boolean :read, default: false

      t.belongs_to :user, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
