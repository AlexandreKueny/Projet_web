class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices, id: :uuid do |t|

      t.string :name
      t.boolean :enabled, default: true
      t.belongs_to :user, type: :uuid, index: true, foreign_key: true

      t.timestamps
    end
  end
end
