class CreateActions < ActiveRecord::Migration[5.1]
  def change
    create_table :actions, id: :uuid do |t|
      t.string :action_type
      t.string :email_object
      t.string :email_text
      t.string :notification_title
      t.string :notification_text
      t.string :notification_level
      t.belongs_to :scenario, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
