class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards, id: :uuid do |t|

      t.string :title
      t.string :card_type
      t.belongs_to :user, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
