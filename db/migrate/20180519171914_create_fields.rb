class CreateFields < ActiveRecord::Migration[5.1]
  def change
    create_table :fields, id: :uuid do |t|
      t.string :name
      t.boolean :enabled, default: true
      t.string :unit
      t.belongs_to :device, type: :uuid, index: true, foreign_key: true

      t.timestamps
    end
  end
end
