class CreateConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :conditions, id: :uuid do |t|
      t.belongs_to :field, type: :uuid, index: true, foreign_key: true
      t.string :field_position
      t.integer :field_threshold
      t.belongs_to :scenario, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
