CREATE OR REPLACE FUNCTION remove_old_values()
  RETURNS TRIGGER AS $body$
BEGIN
  DELETE FROM values
  WHERE created_at < NOW() - INTERVAL '1 weeks' AND field_id = new.field_id;
  RETURN NULL ;
END;
$body$
LANGUAGE plpgsql;

DROP TRIGGER delete_old_values ON values;

CREATE TRIGGER delete_old_values
  AFTER INSERT
  ON values
  FOR EACH ROW EXECUTE PROCEDURE remove_old_values();
