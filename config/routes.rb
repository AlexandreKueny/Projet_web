Rails.application.routes.draw do
  get 'documentation/show'

  devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations',
      passwords: 'users/passwords',
      confirmations: 'users/confirmations',
      unlocks: 'users/unlocks'
  }

  devise_scope :user do
    get '/reset_token', to: 'users/registrations#reset_token'
  end

  resource :dashboard, only: [:show], controller: 'dashboard'
  resources :cards, constraints: lambda {|request| request.xhr?}
  resource :documentation, controller: 'documentation'

  resources :devices, only: [:index, :create, :update, :destroy] do
    resources :fields, only: [:index, :create, :update, :destroy], constraints: lambda {|request| request.xhr? or request.url.match(/\/fields\/[a-zA-Z0-p-]+\/values(\/[a-zA-Z0-p-]+)?.json/)} do
      member do
        delete '/clear', to: 'fields#clear'
      end
      resources :values, only: [:index, :show, :create], constraints: lambda {|request| request.format == :json}
    end
  end

  resources :scenarios
  resources :notifications, only: [:update] do
    delete 'dismiss_all', on: :collection
    get 'history', on: :collection
  end

  root 'home#index'
end